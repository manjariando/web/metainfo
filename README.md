Metainfo Manjariando
========

## Metainfo for the Manjariando blog repository packages

To translate into your language, follow the example below.

### Translate to Portuguese from Brazil

Copy the lines required for translation of the desired metainfo.xml file.

```
<name>Program name</name>
<summary>Program summary</summary>
<p>Program description (this line will be within <description>here</description>)</p>
```

Translate the words inside the angle quotes, and add the acronym for your language.

```
<name xml:lang="pt_BR">Nome do programa</name>
<summary xml:lang="pt_BR">Resumo do programa</summary>
<p xml:lang="pt_BR">Descrição do programa (esta linha estará dentro de <description>aqui</description>)</p>
```

Some programs may contain more lines to be translated than those in the example, if you have any questions, please contact us <contato.manjariando@gmail.com>.

[![pipeline status](https://gitlab.com/manjariando/metainfo/badges/master/pipeline.svg)](https://gitlab.com/manjariando/metainfo/-/commits/master)
